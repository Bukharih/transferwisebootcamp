var request = require('request');
var async = require('async');

// Prefetching currencies & companies asynchronously 
async.parallel({
    currencies: function(callback) {
        request('http://54.229.242.6/currency?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
            if (response && response.statusCode == 200) {
                var currencies = JSON.parse(body);
                callback(null, currencies);
            }
        })
    },
    companies: function(callback) {
        request('http://54.229.242.6/company?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
            if (response && response.statusCode == 200) {
                var companies = JSON.parse(body).companies.slice(1, -1).split(", ");
                callback(null, companies);
            }
        })
    }
}, function(err, results) {
    var currencies = results.currencies;
    var companies = results.companies;
    var amounts = [100, 1000, 10000]

    allCurrencyCombos = getAllCurrencyCombos(currencies);

    for (var i = 1; i < allCurrencyCombos.length; i++) {
        console.log(allCurrencyCombos[i])
        getMarketRate(allCurrencyCombos[i].src, allCurrencyCombos[i].tar, function(srcCurrency, tarCurrency, rate) {
            for (var k = 0; k < amounts.length; k++) {
                amount = amounts[k];
                getQuotes(srcCurrency, tarCurrency, amount, function(quotes, amount) {
                    for (var j = 0; j < companies.length; j++) {
                        var company = companies[j];
                        var quote = quotes[company];
                        quote.company = company;
                        quote.fairRate = rate;

                        console.log(quote);
                        submitPercentage(quote, function(err, quote) {
                            if (!err) {
                                console.log("Just submitted: ", quote.company, quote.sourceAmount, quote.sourceCurrency, quote.targetCurrency, quote.hiddenFeePercentage )
                            }
                        })
                    }
                })
            }
        })

    }
})

// This function works out all the different combos of currencies we can have
function getAllCurrencyCombos(currencies) {
    var allCurrencyCombos = [];
    for (var i = 0; i < currencies.length; i++) {
        for (var j = 0; j < currencies.length; j++) {
            // Ensure there are no same currency combos (e.g. EUR -> EUR)
            if (currencies[i] != currencies[j]) {
                allCurrencyCombos.push({
                    src: currencies[i],
                    tar: currencies[j]
                })
            }

        }
    }
    return allCurrencyCombos;
}

// Fetches market rate for given currencies
function getMarketRate(srcCurrency, tarCurrency, callback) {
    request('http://54.229.242.6/rate/midMarket/' + srcCurrency + '/' + tarCurrency + '?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
        if (response && response.statusCode == 200) {
            var rate = JSON.parse(body).rate
            return callback(srcCurrency, tarCurrency, rate);
        }
    })
}

// Gets quotes for given currencies
function getQuotes(srcCurrency, tarCurrency, amount, callback) {
    request('http://54.229.242.6/quote/' + amount + '/' + srcCurrency + '/' + tarCurrency + '?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
        if (response && response.statusCode == 200) {
            var quotes = JSON.parse(body)
            return callback(quotes, amount);
        }
    })
}

// Calculates the hidden fee percentage
// We workout what the user would have paid using the fair market trade in source currency
// Then we workout out how much extra the difference is compared with the source amount
function calcPercentage(recipientReceives, amount, rate) {
    var percentage = ((amount - (recipientReceives/rate))/amount)*100
    return Math.round(percentage);
}

// Submit percentage to the API
function submitPercentage(quote, callback) {
    quote.hiddenFeePercentage = calcPercentage(quote.recipientReceives, quote.sourceAmount, quote.fairRate);
    request.post({
        url: 'http://54.229.242.6/hiddenFee/forCompany/' + quote.company + '/' + quote.sourceAmount + '/' + quote.sourceCurrency + '/' + quote.targetCurrency + '/' + quote.hiddenFeePercentage + '?token=1033cb8b0ee34a044b3ce5c4ea004ab7'
    }, function(err, response, body) {
        if (JSON.parse(body).result) {
            return callback(null, quote);
        } else {
            return callback(body, null);
        }
    })
}