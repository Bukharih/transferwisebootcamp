var request = require('request');
var async = require('async');

async.parallel({
    history: function(callback) {
        request('http://54.229.242.6/payment/history?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
            if (response && response.statusCode == 200) {
                var history = JSON.parse(body);
                callback(null, history);
            }
        })
    },
    payments: function(callback) {
        request('http://54.229.242.6/payment?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
            if (response && response.statusCode == 200) {
                var payments = JSON.parse(body);
                callback(null, payments);
            }
        })
    }
}, function(err, results) {
    var history = results.history;
    var payments = results.payments;

    var fraud = [];
    var legit = [];

    for (var i = 0; i < history.length; i++) {
        if (history[i].fraud) {
            fraud.push(history[i]);
        } else {
            legit.push(history[i]);
        }
    }

    console.log(fraud)

    for (var i = 0; i < payments.length; i++) {
        var flag = false;
        for (var j = 0; j < fraud.length; j++) {
            var payment = payments[i];
            if (payment.email == fraud[j].email || payment.recipientName == fraud[j].recipientName || payment.ip == fraud[j].ip) {
                flag = true;
            }
        }

        if (flag) {
            request({
                url: 'http://54.229.242.6/payment/' + payment.id + '/fraud?token=1033cb8b0ee34a044b3ce5c4ea004ab7',
                method: 'PUT'
            }, function(error, response, body) {
                console.log(body);
            })
        } else {
            request({
                url: 'http://54.229.242.6/payment/' + payment.id + '/fraud?token=1033cb8b0ee34a044b3ce5c4ea004ab7',
                method: 'DELETE'
            }, function(error, response, body) {
                console.log(body);
            })
        }
    }
})