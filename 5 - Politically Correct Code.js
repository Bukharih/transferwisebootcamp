var request = require('request');

var peps = [
    "Angela Merkel - Germany",
    "Barack Obama - USA",
    "Helmut Kohl - Germany",
    "Elizabeth Alexandra Mary - United Kingdom",
    "Julius Caesar - Italy",
    "Toomas Hendrik Ilves - Estonia",
    "Taavi Rõivas - Estonia",
    "Reuven Rivlin - Israel",
    "Bill Clinton - USA",
    "David Johnston - Canada",
    "Peter Cosgrove - Australia",
    "Dalia Grybauskaitė - Lithuania",
    "Raimonds Vējonis - Latvia",
    "Andrzej Duda - Poland",
    "Borut Pahor - Slovenia",
    "Mariano Rajoy - Spain",
    "Marcelo Sousa - Portugal",
    "Heinz Fischer - Austria",
    "Mikheil Saakashvili - Ukraine",
    "Tony Blair - United Kingdom"
]

formattedPeps = [];

// Formatting the strings into an object to avoid repeating expensive string operations
for (var i = 0; i < peps.length; i++) {
    var split = peps[i].split(" - ");
    formattedPeps.push({
        name: split[0],
        country: split[1]
    })
}

console.log(formattedPeps)

// Get all payments
request('http://54.229.242.6/payment?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
    if (response && response.statusCode == 200) {
        var payments = JSON.parse(body);
        for (var i = 0; i < payments.length; i++) {
            flag = false;
            for (var j = 0; j < formattedPeps.length; j++) {
                // Check if payment belongs to a PEP by ensuring they have the same name & country
                // If so flag them
                if (payments[i].recipientName == formattedPeps[j].name && payments[i].recipientCountry == formattedPeps[j].country) {
                    flag = true;
                }
            }

            // If the payment is flagged then report it as PEP else report it as clear
            if (flag) {
                console.log("GOT OURSELVES A PEP:", payments[i])
                request({
                    url: 'http://54.229.242.6/payment/' + payments[i].id + '/aml?token=1033cb8b0ee34a044b3ce5c4ea004ab7',
                    method: 'PUT'
                }, function(error, response, body) {
                    console.log(JSON.parse(body));
                })
            } else {
                request({
                    url: 'http://54.229.242.6/payment/' + payments[i].id + '/aml?token=1033cb8b0ee34a044b3ce5c4ea004ab7',
                    method: 'DELETE'
                }, function(error, response, body) {
                    console.log(JSON.parse(body));
                })
            }

        }
    }
})