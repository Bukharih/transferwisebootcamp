var chairs = [];

// Quick loop to add all our "chairs" to the array
for (var i = 1; i <= 100; i++) {
  chairs[i-1] = i;
}

// Pattern is skip 1,2,3... hence we need to keep track how many we skip
var skip = 0;
var counter = 0;

// Strategy: Keeping removing elements/chair by following the given pattern
// Eventually, we'll go from 100 to 1 element/chair
while (chairs.length > 1) {
  // Removes chair/element from our array
  removeElement(counter);
  
  skip += 1;
  counter += skip;
  // The counter could be greater than the length of the array 
  // Hence we find the remainder using the modulo operator and start from there
  counter %= chairs.length;
}

// Simple function to remove an element/chair from an array
// Source: http://stackoverflow.com/a/5767335
function removeElement(element){
  chairs.splice(element, 1);
}

console.log(chairs);
