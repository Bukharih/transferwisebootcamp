var request = require('request');
var async = require('async');

// Prefetching payments, banks & bank accounts
async.parallel({
    payments: function(callback) {
        request('http://54.229.242.6/payment?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
            if (response && response.statusCode == 200) {
                var payments = JSON.parse(body);
                callback(null, payments);
            }
        })
    },
    banks: function(callback) {
        request('http://54.229.242.6/bank?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
            if (response && response.statusCode == 200) {
                var banks = JSON.parse(body);
                callback(null, banks);
            }
        })
    },
    bankAccounts: function(callback) {
        request('http://54.229.242.6/bankAccount?token=1033cb8b0ee34a044b3ce5c4ea004ab7', function(error, response, body) {
            if (response && response.statusCode == 200) {
                var bankAccounts = JSON.parse(body);
                callback(null, bankAccounts);
            }
        })
    }
}, function(err, results) {

    var payments = results.payments;
    var banks = results.banks;
    var bankAccounts = results.bankAccounts;


    for (var i = 0; i < payments.length; i++) {
        console.log('Working on payment ' + payments[i].id);

        getSrcBankDetails(banks, bankAccounts, payments[i], function(err, srcBankName, srcAccNumber, payment) {
            getTarBankDetails(banks, payment, function(err, tarBankName, tarAccNumber) {
                var amount = payment.amount;
                makePayment(srcBankName, srcAccNumber, tarBankName, tarAccNumber, amount, function(err) {
                    if (!err) {
                        console.log('Payment ' + payment.id + ' was successful!')
                    } else {
                        console.log("Error: ", err)
                    }
                })
            })
        });

    }

});


// Gets details of the right source bank account owned by us for the currency
function getSrcBankDetails(banks, bankAccounts, payment, callback) {
    for (var j = 0; j < bankAccounts.length; j++) {
        if (bankAccounts[j].accountName == "TransferWise Ltd" && bankAccounts[j].currency == payment.sourceCurrency) {
            for (var i = 0; i < banks.length; i++) {
                // Lookup the bank's name
                if (banks[i].id == bankAccounts[j].bankId) {
                    return callback(null, banks[i].name, bankAccounts[j].accountNumber, payment)
                }
            }
        }
    }
}

// Gets details for the correct target bank by looking up the recipientBankId
function getTarBankDetails(banks, payment, callback) {
    for (var i = 0; i < banks.length; i++) {
        if (banks[i].id == payment.recipientBankId) {
            return callback(null, banks[i].name, payment.iban)
        }
    }
}

// Submits payment to bank
function makePayment(srcBankName, srcAccNumber, tarBankName, tarAccNumber, amount, callback) {
    request.post({
        url: 'http://54.229.242.6/bank/' + srcBankName + '/transfer/' + srcAccNumber + '/' + tarBankName + '/' + tarAccNumber + '/' + amount + '?token=1033cb8b0ee34a044b3ce5c4ea004ab7'
    }, function(err, response, body) {
        if (body == 'ok!') {
            return callback(null);
        } else {
            return callback(body);
        }
    })
}